<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#queryString").autocomplete("AuotComplete", {
		/*
		*autocomplete插件 常用参数说明:
		
		* minChars (Number): 在触发autoComplete前用户至少需要输入的字符数.Default: 1

		* delay (Number): 击键后激活autoComplete的延迟时间(单位毫秒).Default: 远程为400 本地10

		* cacheLength (Number): 缓存的长度.即对从数据库中取到的结果集要缓存多少条记录.设成1为不缓存.Default: 10

		* matchSubset (Boolean): autoComplete可不可以使用对服务器查询的缓存,作者举了个例子是如果缓存对foo的查询结果,那么如果用户输入foot就不需要再进行检索了,直接使用缓存,好处多多啊.通常是打开这个选项以减轻服务器的负担以提高性能.只会在缓存长度大于1时有效.Default: true

		* matchCase (Boolean): 比较是否开启大小写敏感开关.使用缓存时比较重要.如果你理解上一个选项,这个也就不难理解,就好比foot要不要到FOO的缓存中去找.Default: false

		* matchContains (Boolean): 决定是比较时是否要比较字符串内部查看匹配,如ba是否与foo bar中的ba匹配.使用缓存时比较重要.不要和autofill混用.Default: false

		* mustMatch (Booolean): 如果设置为true,autoComplete只会允许匹配的结果出现在结果时,所有当用户输入的是非法字符时将会得不到下拉框.Default: false

		* extraParams (Object): 为后台(一般是服务端的脚本)提供更多的参数.和通常的作法一样是使用一个键值对对象.如果传过去的值是{ bar:4 },将会被autocompleter解析成my_autocomplete_backend.php?q=foo&bar=4 (假设当前用户输入了foo). Default: {}

		* selectFirst (Boolean): 如果设置成true,在用户键入tab或return键时autoComplete下拉列表的第一个值将被自动选择,尽管它没被手工选中(用键盘或鼠标).当然如果用户选中某个项目,那么就用用户选中的值. Default: true

		* formatItem (Function): 首先要明白这个选项的值是一个函数的句柄或指针.函数的作用是为每个要显示的项目使用高级标签.即对结果中的每一行都会调用这个函数,返回值将用LI元素包含显示在下拉列表中. Autocompleter会提供三个参数: 返回的结果数组, 当前处理的行数(即第几个项目,是从1开始的自然数), 当前结果数组元素的个数即项目的个数. Default: none, 表示不指定自定义的处理函数,这样下拉列表中的每一行只包含一个值.(formatItem作用在于可以格式化列表中的条目)
								 
		* formatMatch (Function):是配合formatItem使用，作用在于，由于使用了formatItem，所以条目中的内容有所改变，而我们要匹配的是原始的数据，所以用formatMatch做一个调整，使之匹配原始数据
		
		* formatResult (Function): 和formatItem类似,但可以将将要输入到input文本框内的值进行格式化.同样有三个参数,和formatItem一样.Default: none,表示要么是只有数据,要么是使用formatItem提供的值.(formatResult是定义最终返回的数据，比如我们还是要返回原始数据，而不是formatItem过的数据)

		* multiple (Boolean): 是否允许输入多个值即多次使用autoComplete以输入多个值. Default: false

		* multipleSeparator (String): 如果是多选时,用来分开各个选择的字符. Default: “,“

		* width (Number): 指定下拉框的宽度. Default: input元素的宽度

		* autoFill (Boolean): 要不要在用户选择时要不要自动将用户当前鼠标所在的值填入到input框.并在用户继续输入 Default: false

		* max (Number): autoComplete下拉显示项目的个数.Default: 10
		*/
		
		matchSubset:false,
		multiple: true,  
		width:350,  
		selectFirst:false,   
		max:10, 
		multipleSeparator: ' ',    
		dataType: 'json',
		extraParams: {   
			queryString: function() { return $("#queryString").val(); } 
		},     
		  //加入对返回的json对象进行解析的函数，函数返回一个数组       
		parse: function(data) { 
			var rows = [];   
		    for(var i=0; i<data.length; i++){   
		    	rows[rows.length] = {    
		       // data:dat[i].title+"/"+data[i].title,//显示在下拉提示框中的内容   
		        data:data[i].title,
		        value:data[i].title,    
		        result:data[i].title//显示在输入文本框里的内容 ,
		    	};    
			} 
		   	return rows;   
		},   
		formatItem: function(data, i, n) {
			return data;         
		},
		formatResult: function(data) {
            return data;
        }   
	});    
});
</script>


  
 <body>
<div align="center">  
<b>用户搜索</b> 
<form action="" method="post"> 
<input type="text" id="queryString" name="queryString" />  
<input type="submit" value="搜索" />  (try "a"-"z" or "一")
</form>
</div> 

</body>
</html>
