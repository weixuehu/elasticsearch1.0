package com.elasticsearch.pojo;

public class Pager {
	private int offset;
	private int maxPageItems;
	private String pageNumber;
	private int defaultMaxPageItems;
	private String submit;

	/**总记录数**/
	private long recordTotal;
	
	public long getRecordTotal() {
		return recordTotal;
	}
	public void setRecordTotal(long recordTotal) {
		this.recordTotal = recordTotal;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public int getDefaultMaxPageItems() {
		if(defaultMaxPageItems==0) {
			setDefaultMaxPageItems(10);
		}
		return defaultMaxPageItems;
	}
	public void setDefaultMaxPageItems(int defaultMaxPageItems) {
		this.defaultMaxPageItems = defaultMaxPageItems;
	}
	public int getMaxPageItems() {
		if(maxPageItems==0) 
			maxPageItems=defaultMaxPageItems;
		return maxPageItems;
	}
	public void setMaxPageItems(int itemCount) {
		this.maxPageItems = itemCount;
	}
	public String getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getSubmit() {
		return submit;
	}
	public void setSubmit(String submit) {
		this.submit = submit;
	}
}
