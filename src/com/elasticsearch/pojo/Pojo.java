package com.elasticsearch.pojo;

/**
 * 功能：检索结果实体bean 作者：kangjie 时间：2011-9-20下午02:48:50
 */
public class Pojo {

	private String id;

	private String name;

	private String title;

	private float price;

	private String simple;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getSimple() {
		return simple;
	}

	public void setSimple(String simple) {
		this.simple = simple;
	}
}
